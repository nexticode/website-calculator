import React, { useState } from 'react';
import Context from '../config/Context'
import FakeContext from '../content/fake-context.json'

import { IoIosArrowDown } from 'react-icons/io'
import { IoIosArrowUp } from 'react-icons/io'

export default function Accordion(props) {

    const context = React.useContext(Context)

    const [isVisible, setVisible ] = useState(false)

    const toggleAccordion = (e) => {
        e.preventDefault()
        setVisible(!isVisible)
    }

    return (
        <div className="flex flex-col">
            <div className="w-full max-w-screen-xl mx-auto">
                <div className="mb-8 bg-white">
                    <div className="">
                    <div className="mx-auto max-w-sm overflow-hidden border-b">
                        <div className="sm:flex sm:items-center">
                        <div className="flex-grow">
                            <div className="flex">
                                <h3 className="font-normal px-2 py-3 leading-tight text-left">Summary</h3>
                                    <div className="flex-grow px-2 py-3 text-right">
                                        <button onClick={toggleAccordion} className="py-1 px-4">
                                                {!isVisible && <IoIosArrowDown />}
                                                {isVisible && <IoIosArrowUp />}
                                        </button>
                                </div>
                            </div>
                            { isVisible && 
                            <div className="w-full pb-4">
                        
                                {context.selected.length == (context.steps.length - 1) && context.selected.slice(1).map((item , k) => {
                                    
                                    return(
                            
                                            <div className="flex cursor-pointer my-1 pb-8 hover:bg-blue-lightest rounded" k={k}>
                                                <div className="w-3/5 h-10 py-3 px-1">
                                                    <p className="hover:text-blue-dark">{context.steps[item.step].name}</p>
                                                </div>
                                                <div className="w-1/5 h-10 text-right p-3">
                                                    <p className="text-sm text-grey-dark">{item.title}</p>
                                                </div>
                                                <div className="w-1/5 h-10 text-right p-3">
                                                    <p className="font-bold text-teal-500">${item.price}</p>
                                                </div>
                                            </div>
                                    )
                                })
                                }
                                </div>
                            }
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    )
}