import React from 'react'
import { IoIosArrowRoundBack } from 'react-icons/io'
import Context from '../config/Context'
import Accordion from '../components/Accordion'

export default function FormStep(props) {

    const context = React.useContext(Context)
    const range = context.total*1.2

    return (
        <div className="max-w-3xl rounded overflow-hidden shadow-lg mb-4 py-12 bg-white">
            <div className="flex flex-col sm:flex-row">
                <form action="https://formcarry.com/s/F6OQpnQHJt8" method="POST" acceptCharset="UTF-8" >
                    <div className="w-full p-2 sm:p-8 lg:p-12 items-center justify-center order-2 md:order-1">
                        <h2 className="font-bold text-gray-700 text-base pb-12 text-2xl text-center">{props.name}</h2>
                        <h1 className="font-bold text-center text-2xl lg:text-4xl mb-2 pb-4 text-teal-500">${context.total} - ${range}</h1>
                        <Accordion 
                            {...props}
                        />
                        <div className="text-center pt-10">
                            <p className="text-gray-700 text-base pb-8 px-12">{props.description}</p>
                        </div>
                        <div className="w-full px-3 md:px-24">
                            {context.selected.length == (context.steps.length - 1) && context.selected.slice(1).map((item, k) => {
                                const value = context.steps[item.step].name + " " + item.title
                                const optionName = 'Step' + k
                                return (
                                        <input type="hidden" name={optionName} value={value} />
                                )
                            })
                            }
                            <div className="w-full py-3">
                                <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="grid-last-name">
                                    Name
                                </label>
                                <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-last-name" type="text" placeholder="Name" name="name" required />
                            </div>
                            <div className="w-full py-3">
                                <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="grid-last-name">
                                    Email
                                </label>
                                <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-last-name" type="email" placeholder="Email" name="email" required />
                            </div>
                            <div className="w-full py-3">
                                <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="grid-last-name">
                                    Additional notes
                                </label>
                                <textarea className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-last-name" type="text" placeholder="Tell us a bit more about your project" name="description" rows="10" />
                            </div>
                            <div className="w-full">
                                <button className="bg-teal-500 hover:bg-teal-700 text-white font-bold py-2 px-4 rounded">
                                    Submit request
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div className="px-12 justify-between text-gray-700 flex flex-wrap justify-between">
                <button onClick={props.previousStep} className="text-4xl hover:text-teal-500"><IoIosArrowRoundBack /></button>
            </div>
        </div>
    )
}