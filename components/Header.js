import Context from '../config/Context'
import { IoLogoTwitter, IoLogoFacebook, IoMdMailUnread } from 'react-icons/io'

export default function Header(props) {

    const context = React.useContext(Context)

    return (
        <nav className="fixed flex w-full items-center justify-between flex-wrap p-6 z-10 bg-indigo-700">
            <div className="flex items-center flex-shrink-0 text-white mr-6">
                <span className="font-semibold text-xl tracking-tight">Website cost by <a className="cursor-pointer hover:text-teal-500" href="https://nexticode.com" target="_blank">Nexticode</a></span>
                <div className="pl-8 text-2xl">
                    <a href="https://twitter.com/intent/tweet?url=https%3A%2F%2Fwebsitecostcalculator.com%2F&text=Check%20out%20this%20awesome%20website%20cost%20calculator%20I%20found!">
                        <IoLogoTwitter />
                    </a>
                </div>
                <div className="pl-2 text-2xl">
                    <a href="https://www.facebook.com/sharer/sharer.php?u=https://websitecostcalculator.com/">
                        <IoLogoFacebook />
                    </a>
                </div>
                <div className="pl-2 text-2xl">
                    <a href="mailto:info@example.com?&subject=&body=https://websitecostcalculator.com/ Check out this awesome website cost calculator I found!">
                        <IoMdMailUnread />
                    </a>
                </div>
            </div>
            <div className="block flex-grow lg:flex lg:items-center lg:w-auto">
                <div className="hidden">
                    <a href="#" className="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-teal-500 hover:bg-white mt-4 lg:mt-0">Download</a>
                </div>
            </div>
            <div className="price">
            <div className="text-white text-3xl"> ${context.total}</div>
            </div>
        </nav>
    )
}