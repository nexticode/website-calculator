import Option from '../components/Option'
import React, { useState } from 'react'

export default function InitialStep(props) {

    const [active, setActive] = useState({});

    return (
        <div className="max-w-3xl rounded overflow-hidden shadow-lg mb-4 py-12 bg-white">
            <div className="flex flex-col sm:flex-row">
                <div className="w-full md:w-1/2 p-12 items-center justify-center order-2 md:order-1">
                    <div className="w-full">
                        <h1 className="font-bold text-2xl mb-2 pb-4">{props.name}</h1>
                        <p className="text-gray-700 text-base pb-8">{props.description}</p>
                    </div>
                    <div className="w-full">
                        {props.options.map((option, k) => (
                            <Option
                                key={k}
                                step={props.step}
                                {...option}
                                active={active.title === option.title}
                                setActive={setActive}
                                nextStep={props.nextStep}
                            />
                        ))}
                    </div>
                </div>
                <div className="w-full md:w-1/2 p-12 sm:order-1 order-1 md:order-2">
                    <img className="object-contain h-64 w-full" src={props.img} />
                </div>
            </div>
        </div>
    )
}