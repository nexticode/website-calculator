import React from 'react'
import Header from '../components/Header'
import Footer from '../components/Footer'
import { initGA, logPageView } from '../utils/analytics'

export default class Layout extends React.Component {
    
    componentDidMount() {
        if (!window.GA_INITIALIZED) {
            initGA()
            window.GA_INITIALIZED = true
        }
        logPageView()
    }

    render() {
        return (   
            <div className="antialiased text-gray-900 overflow-scroll bg-indigo-700">
                <Header/>
                <div className="flex h-screen justify-center">
                    <div className="m-auto pt-8 overflow-hidden">
                        {this.props.children}
                    </div>
                </div>
                <Footer />
            </div>
        )
    }
}