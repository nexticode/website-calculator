import React from 'react'
import Context from '../config/Context'

export default function Option (props){

    const context = React.useContext(Context)
    let classes = [ 'inline-block', 
                    'bg-gray-200', 
                    'hover:bg-teal-400',
                    'hover:text-white',
                    'rounded-full', 
                    'px-4', 
                    'py-1', 
                    'text-xl', 
                    'font-semibold',
                    'text-gray-700',
                    'mr-2'];
    
    if(props.active){
        classes.push("bg-teal-400")
    }
        
    const incrementPrice = () => {
        props.setActive({ title: props.title, price: props.price })
        context.setPrice( {...props} )
        props.nextStep()
    }
 
    return (
        <button onClick={incrementPrice} className={classes.toString().split(",").join(" ") } >
            { props.title }
        </button>

    )
    
}