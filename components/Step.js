import Option from '../components/Option'
import FormStep from '../components/FormStep'
import RegularStep from '../components/RegularStep'
import InitialStep from '../components/InitialStep'
import React, {useState} from 'react'

export default function Step(props){

    const [active, setActive] = useState({});

    return(
        <div className="xs:my-12 mx-4 ">
            { props.type === 'initial' && 
                <InitialStep 
                    {...props}
                />
            }
            { props.type === 'step' && 
                <RegularStep
                    {...props}
                />
            }
            { props.type === 'form' && 
                <FormStep
                    {...props}
                />
            }
        </div>  
    )
}