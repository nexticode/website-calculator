import React, { Component } from 'react'
import Context from '../config/Context'
import steps from '../content/steps.json'

class ContextProvider extends Component {
    constructor(props) {
        super(props);

        this.state = {
           total : 0,
           selected : [],
           steps : steps
        };
    }

    setPrice = (option) => {

        let currentPrice = this.state.total
        let selected = this.state.selected

        if (this.state.selected[[option.step]] !== undefined){
            currentPrice = currentPrice - this.state.selected[[option.step]].price
        }

        currentPrice = currentPrice + option.price
        selected[option.step] = option
    
        this.setState({
             total : currentPrice,
             selected : selected 
        })
    }

    render() {
        return (
            <Context.Provider value={{ ...this.state , setPrice : this.setPrice }}>
                {this.props.children}
            </Context.Provider>
        );
    }
}

export default ContextProvider;