import React from 'react'
import App from 'next/app'
import '../static/css/tailwind.css'
import ContextProvider from '../helpers/ContextProvider';

class MyApp extends App {

    render() {

        const { Component, pageProps } = this.props;

        return (
            <ContextProvider>
                <Component {...pageProps} />
            </ContextProvider>
        );
    }
}

export default MyApp