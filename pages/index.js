import Layout from '../components/Layout'
import Step from '../components/Step'
import StepWizard from 'react-step-wizard'
import Context from '../config/Context'

export default function Index(props) {

    const context = React.useContext(Context)

    return (
        <Layout>
            <StepWizard>
                {context.steps.map((step, k) => (
                        <Step 
                            key={k}
                            step={k}
                            {...step}
                        />
                ))}
            </StepWizard>
        </Layout>
    );
}